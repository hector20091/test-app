import { Component } from '@angular/core';
//import { HttpModule, } from '@angular/http';
import 'rxjs/add/operator/map';
import { HomeService } from '../../services/home.service';
import { DeviceService } from '../../services/device.service';
import { Device } from '@ionic-native/device';
import {Http} from '@angular/http';
import {Platform} from 'ionic-angular';

/*declare let networkinterface: any;*/

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
    deviceDbId;
    displayConnectButton:boolean;
    public_ip;
    uuid;

  constructor(
      private http: Http,
      private platform: Platform,
      private device: Device,
      private homeService: HomeService,
      private deviceService: DeviceService,
  ) {
      this.public_ip = '';

      this.platform.ready().then(
          () => {
              console.log(this.platform);
            this.uuid = this.device.uuid;
          }
      );

  }

    ngOnInit() {
        this.connectButtonDisplay();
        this.getMyIpAddress()
    }

    getMyIpAddress() {
        this.http.get("https://httpbin.org/ip")
            .subscribe(data => {
                this.public_ip = data.json().origin;
            }, error => {
                //console.log(JSON.stringify(error.json()));
            });
    }


  connect() {
      let body:any = {
          device_id: this.uuid,
          public_ip: this.public_ip
      };

      this.homeService.connectDevice(JSON.stringify(body)).subscribe(
          (res) => {
              this.deviceService.saveId(res.id).then(() => this.connectButtonDisplay());
          },
          (err) => {
              alert(err);
          }
      )
  }

  disconnect() {
      this.deviceService.getId().then(
          (id) => {
              let body:any = {
                  id: id
              };

              this.homeService.disconnectDevice(JSON.stringify(body)).subscribe(
                  (res) => {
                      this.deviceService.clear();
                      this.connectButtonDisplay();
                  }
              )
          });
  }

  connectButtonDisplay() {
      this.deviceService.getId().then(
          (id) => {
              if(id != null) {
                  this.deviceDbId = id;
                  this.displayConnectButton = false;
              } else {
                  this.displayConnectButton = true;
              }
          }
      );
  }
}
