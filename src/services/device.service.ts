import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

@Injectable()
export class DeviceService {
    constructor (
        private storage: Storage
    ) {}

    saveId (id:string) {
        return this.storage.set('device_id',id)
    }

    getId() {
        return this.storage.get('device_id');
    }

    clear() {
        return this.storage.remove('device_id');
    }
}