import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeService {

    body;
    headers;
    api_url:string = '';

    constructor (
        private http: Http
    ) {
        //this.api_url = 'http://localhost/testAppServer/api/';
        this.api_url = 'http://dev.webapps.studio/testAppServer/api/';

        this.headers = new Headers({
            'Content-Type': 'application/json',
            //'Content-Type': 'application/x-www-form-urlencoded'
        });
    }

    connectDevice(body?) {
        return this.http.post(this.api_url + '?action=connect',body,this.headers).map((res:Response) => res.json());
    }

    disconnectDevice(body?) {
        return this.http.post(this.api_url + '?action=disconnect',body, this.headers).map((res:Response) => res.json());
    }

}